import React from 'react';
import SigninForm from '../../components/signinform/signin.component';
import SignupForm from '../../components/signupform/signup.component';
import './signup.style.scss';

const Signup = () => (
    <div className="sign-in-and-sign-up">
        <SigninForm/>
        <SignupForm/>
    </div>
);

export default Signup;