import React from 'react';
import Directory from '../../components/directory/directory.component';

import './homepage.style.scss';

const HomePage = p => {
    return (
        <div className="layout-container">
            <Directory/>
        </div>
    );
};

export default HomePage;