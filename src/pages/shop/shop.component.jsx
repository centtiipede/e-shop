import React, { Component } from 'react';
import ShopData from './shop.data';
import PreviewCollection from '../../components/preview-collection/previewcollection.component';
import './shop.style.scss';

class ShopPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            collections: ShopData,
        }
    }

    render() {
        const {collections} = this.state
        return (
            <div className="shop-page">
                {
                    collections.map(({id, ...restProps}) => (
                        <PreviewCollection
                            key={id}
                            {...restProps}
                        ></PreviewCollection>
                    ))
                }
            </div>
        );
    }
}

export default ShopPage;