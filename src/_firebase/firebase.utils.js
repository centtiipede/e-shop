import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyCHGc1aan6_MajYkXYFQNHDYOkPVdEta1E",
    authDomain: "e-crwn-db.firebaseapp.com",
    databaseURL: "https://e-crwn-db.firebaseio.com",
    projectId: "e-crwn-db",
    storageBucket: "e-crwn-db.appspot.com",
    messagingSenderId: "24413902832",
    appId: "1:24413902832:web:cdcd78c6289525bc"
};

firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

export const createUser = async (user, others) => {
    if (!user) return;

    const userRef = firestore.doc(`users/${user.uid}`);
    const snapshot = await userRef.get();

    if (!snapshot.exists) {
        const { displayName, email } = user;
        const date = new Date();

        try {
            await userRef.set({
                displayName, email, date, ...others,
            });
        } catch (err) {
            console.log('Error creating user', err.message);
        }
    }

    return userRef;
};

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
export const googleSignin = () => auth.signInWithPopup(provider);

export default firebase;
