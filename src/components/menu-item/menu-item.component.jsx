import React from 'react';
import { withRouter } from 'react-router-dom';
import './menu-item.style.scss';

const MenuItem = ({ title, subtitle, imageUrl, linkUrl, history, match }) => {
    return (
        <div
            className="menu-item"
            onClick={
                () => history.push(`${match.url}${linkUrl}`)
            }
        >
            <div
                className="bg-img"
                style={{
                    backgroundImage: `url(${imageUrl})`,
                }}
            ></div>
            <div className="content">
                <h1 className="content-title">{title.toUpperCase()}</h1>
                <span className="content-subtitle">{subtitle}</span>
            </div>
        </div>
    );
};

export default withRouter(MenuItem);