import React from 'react';
import FormInput from '../form-input/forminput.component';
import CustomButton from '../custom-button/custombutton.component';
import { auth, googleSignin } from '../../_firebase/firebase.utils';
import './signin.style.scss';

class Signin extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
        };
    }

    handleSubmit = async e => {
        e.preventDefault();

        const { email, password } = this.state;

        try {
            await auth.signInWithEmailAndPassword(email, password);
            this.setState({ email: '', password: '', });
        } catch (err) {
            console.log(err)
        }
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState({
            [name]: value,
        });
    }

    render() {
        const { email, password } = this.state;
        return (
            <div className="sign-in">
                <h2>I already have an account</h2>
                <span>Sign in with your email an password</span>

                <form>
                    <FormInput
                        name="email"
                        type="email"
                        label="email"
                        value={email}
                        handleChange={this.handleChange}
                    />

                    <FormInput
                        name="password"
                        type="password"
                        label="password"
                        value={password}
                        handleChange={this.handleChange}
                    />

                    <div className="buttons">
                        <CustomButton
                            onClick={this.handleSubmit}
                            // onClick={() => console.log('hell')}
                        >
                            Sign in
                        </CustomButton>

                        <CustomButton
                            onClick={googleSignin}
                            googleSignin
                        >
                            Sign in with Google
                        </CustomButton>
                    </div>
                </form>
            </div>
        );
    }
};

export default Signin;