import React from 'react';
import FormInput from '../form-input/forminput.component';
import CustomButton from '../custom-button/custombutton.component';
import { auth, createUser } from '../../_firebase/firebase.utils';
import './signup.style.scss';

class Signup extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            displayName: '',
            email: '',
            password: '',
            confirmPassword: '',
        };
    }

    handleSubmit = async e => {
        e.preventDefault();
        const { displayName, email, password, confirmPassword } = this.state;

        if (!password || !confirmPassword || password !== confirmPassword) {
            alert("Passwords doesn't match");
            return;
        }

        try {
            const { user } = await auth.createUserWithEmailAndPassword(email, password);
            await createUser(user, { displayName });
            this.setState({
                displayName: '',
                email: '',
                password: '',
                confirmPassword: '',
            });
        } catch (err) {
            console.error(err)
        }
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState({
            [name]: value,
        });
    }

    render() {
        const { displayName, email, password, confirmPassword } = this.state;
        return (
            <div className="sign-up">
                <h2>I do not have an account</h2>
                <span>Sign up with your email an password</span>

                <form
                    className="sign-up-form"
                >
                    <FormInput
                        name="displayName"
                        type="name"
                        label="name"
                        value={displayName}
                        handleChange={this.handleChange}
                    />

                    <FormInput
                        name="email"
                        type="email"
                        label="email"
                        value={email}
                        handleChange={this.handleChange}
                    />

                    <FormInput
                        name="password"
                        type="password"
                        label="password"
                        value={password}
                        handleChange={this.handleChange}
                    />

                    <FormInput
                        name="confirmPassword"
                        type="password"
                        label="confirm password"
                        value={confirmPassword}
                        handleChange={this.handleChange}
                    />

                    <div className="buttons">
                        <CustomButton
                            onClick={this.handleSubmit}
                        >
                            Sign up
                        </CustomButton>
                    </div>
                </form>
            </div>
        );
    }
};

export default Signup;