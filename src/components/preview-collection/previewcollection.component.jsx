import React from 'react';
import CollectionItem from '../../components/collection-item/collectionitem.component';
import './previewcollection.style.scss';

const PreviewCollection = ({
    title, items
}) => (
        <div className="collection-preview">
            <h1>{title}</h1>
            <div className="preview">
                {
                    items
                        .filter((item, i) => i < 4)
                        .map(({ id, ...otherProps }) => (
                            // <div key={id}>{name}</div>
                            <CollectionItem
                                key={id}
                                {...otherProps}
                            />
                        ))
                }
            </div>
        </div>
    );

export default PreviewCollection;