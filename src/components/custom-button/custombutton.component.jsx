import React from 'react';
import './custombutton.style.scss';

const CustomButton = ({ children, googleSignin, ...props }) => (
    <div
        className={`${googleSignin ? 'google-sign-in' : ''} custom-button`}
        {...props}
    >
        {children}
    </div>
);

export default CustomButton;