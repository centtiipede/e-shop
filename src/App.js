import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import { auth, createUser } from './_firebase/firebase.utils';
import { setCurrentUser } from './redux/user/user.actions';

import Header from './components/header/header.component';

import HomePage from './pages/homepage/homepage.component';
import ShopPage from './pages/shop/shop.component';
import Signup from './pages/signup/signup.component';

import './App.css';


class App extends Component {
	unsubscribeFromAuth = null;

	componentWillMount() {
		const { setCurrentUser } = this.props;
		this.unsubscribeFromAuth = auth.onAuthStateChanged(async u => {
			if (u) {
				const uref = await createUser(u);

				uref.onSnapshot(snap => {
					setCurrentUser({
						id: snap.id,
						...snap.data(),
					})
				});
			} else {
				setCurrentUser(u);
			}
		});
	}

	componentWillUnmount() { this.unsubscribeFromAuth(); }

	render() {
		// const { currentUser } = this.state;
		return (
			<div className="App">
				<Header />
				<Switch>
					<Route exact path="/" component={HomePage} />
					<Route exact path="/shop" component={ShopPage} />
					<Route exact path="/signin" component={Signup} />
				</Switch>
			</div>
		);
	}
}

const mapDispatchToProps = dispatch => ({
	setCurrentUser: u => dispatch(setCurrentUser(u))
});

export default connect(null, mapDispatchToProps)(App);
